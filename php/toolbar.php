<div id="toolbar">

	<div id="breadcrumb">
	<?php 
		echo $controller->breadcrumb();

		if($controller->getGroup()->getProperty("permissions") & PERMISSION_ADMIN) {
			echo "<a class='admin-link' href='index.php?page=admin'>Administrate</a>";
		}
	?>
	</div>

	<div class="user-log-info">
			<?php 
				if($controller->is_loaded()) {
					echo "<span>Logged as <a href='index.php?page=profile&id=".$controller->getProperty('id')."'>".$controller->getProperty('login')."</a></span>";
					echo '&nbsp;&nbsp; <a href="'.$_SERVER['PHP_SELF'].'?method=logout">logout</a>';
					
				}
			?>
	</div>

</div>

