<?php

$user_id					= "";
$group_id					= "";
$a_user						= NULL;
$login 						= NULL;
$password 					= NULL;
$fullname 					= NULL;
$email 						= NULL;
$group 						= NULL;
$groups 					= NULL;

// edit group case
if (isset($_GET['row_id'])) {
	$user_id 	= $_GET['row_id'];
	$a_user 	= $controller->getUsers()->getObjectWithID($user_id);
	$group_id	= $a_user->getProperty("group_id");
	$user_group	= $controller->getGroups()->getObjectWithID($group_id);

	$groups		= $controller->getGroups()->getData();
		
	$login		= $a_user->getProperty("login");
	$password	= $a_user->getProperty("password");
	$fullname	= $a_user->getProperty("fullname");
	$email		= $a_user->getProperty("email");
}

$disabled 	= (!$controller) ? "disabled=true" : "";
$method 	= (!$controller) ? "new_user" : "save_user";

?>

<h3>Edit User</h3>
<br />

<form>
	<table class="form">

		<tr height='30'>
			<td class="first rowhead" width="300"><label>Login:</label></td>
			<td class="value"><input type="text" name="name" value="<?php echo $login; ?>" /></td>
		</tr>
		<tr height='30'>
			<td class="first rowhead" width="300"><label>Password:</label></td>
			<td class="value"><input type="password" name="name" value="<?php echo $password; ?>" /></td>
		</tr>
		<tr height='30'>
			<td class="first rowhead" width="300"><label>Fullname:</label></td>
			<td class="value"><input type="text" name="name" value="<?php echo $fullname; ?>" /></td>
		</tr>
		<tr height='30'>
			<td class="first rowhead" width="300"><label>Email:</label></td>
			<td class="value"><input type="text" name="name" value="<?php echo $email; ?>" /></td>
		</tr>
		<tr height='30'>
			<td class="first rowhead" width="300"><label>Group:</label></td>
			<td class="value">
				<select>
				<?php 
					foreach ($groups as $value) {
						if($value->getProperty('name') == $user_group->getProperty('name'))
							echo "<option selected>".$value->getProperty('name')."</option>";
						else
							echo "<option>".$value->getProperty('name')."</option>";
					}
				?>
				</select>	
			</td>
		</tr>
	</table>

	<br>
	<input type="submit" value="Save" />
	<input type="button" <?php echo $disabled; ?> value="Delete" onClick="location.href='index.php?page=admin&section=users&method=delete_user&id=<?php echo $group_id; ?>'" />
	<input type="hidden" name="id" value=<?php echo $group_id; ?> />
	<input type="hidden" name="page" value="admin" />
	<input type="hidden" name="section" value="users" />
	<input type="hidden" name="method" value=<?php echo $method; ?> />
</form>
