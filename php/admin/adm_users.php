<?php  

	$users = $controller->getUsers()->getData();
	$columns = array(
     	'id',
     	'login',
     	'fullname',
     	'email',
     	'group_id'
	);

	if(isset($_GET["action"]) && strlen($_GET["action"])) {
		include("php/admin/adm_users_edit.php");
	} else {
		?>

		<h3>Users</h3>
		<br />

		<table id="adm-table">
			<?php
				/* Generate table */
				echo "<thead>";
				echo "<tr>";
				foreach($columns as $column) {
					echo "<th>".$column."</th>";
				}
				echo "</tr>";
				echo "</thead>";
			
				echo "<tbody>";
				$count = 0;
				foreach ($users as $key => $value) {

					$title = "id='".intval($value->getProperty("id"))."'";
					echo "<tr ".$title." title='users'>";

					foreach($columns as $column) {
						echo "<td>".$value->getProperty($column)."</td>";
					}
					echo "</tr>";

					$count++;
				}
				echo "</tbody>";
			?>
		</table>

		<?php
	}
?>