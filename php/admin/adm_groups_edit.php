<?php

$group 						= NULL;
$group_name					= "";
$group_id					= "";

$PERMISSION_READ 			= "";
$PERMISSION_UPDATE 			= "";
$PERMISSION_ADD 			= "";
$PERMISSION_DELETE 			= "";
$PERMISSION_ADMIN			= "";

// edit group case
if (isset($_GET['row_id'])) {
	$group_id 	= $_GET['row_id'];
	$group 		= $controller->getGroups()->getObjectWithID($group_id);
	$group_name = $group->getProperty("name");

	$PERMISSION_READ 			= ($group->getProperty("permissions") & PERMISSION_READ) 			? "checked" : "";
	$PERMISSION_UPDATE	 		= ($group->getProperty("permissions") & PERMISSION_UPDATE) 			? "checked" : "";
	$PERMISSION_ADD 			= ($group->getProperty("permissions") & PERMISSION_ADD) 			? "checked" : "";
	$PERMISSION_DELETE 			= ($group->getProperty("permissions") & PERMISSION_DELETE) 			? "checked" : "";
	$PERMISSION_ADMIN 			= ($group->getProperty("permissions") & PERMISSION_ADMIN) 			? "checked" : "";
}

$disabled 	= (!$group) ? "disabled=true" : "";
$method 	= (!$group) ? "new_group" : "save_group";

if($group)
	echo "<h3>Edit Group</h3>";
else
	echo "<h3>Add Group</h3>";

?>

<br />

<form method="get" action="index.php">
	<table class="form">

		<tr height='30'>
			<td class="first rowhead" width="300"><label>Name:</label></td>
			<td class="value"><input type="text" name="name" value="<?php echo $group_name; ?>" /></td>
			<td></td>
		</tr>
		 <tr>
			<td class="first rowhead"><label>READ:</label></td>
			<td class="value"><input type="checkbox" name="read" <?php echo $PERMISSION_READ; ?> /></td>
			<td>User of this group can read every public data.</td>
		</tr>	
		 <tr>
			<td class="first rowhead"><label>UPDATE:</label></td>
			<td class="value"><input type="checkbox" name="update_own" <?php echo $PERMISSION_UPDATE; ?> /></td>
			<td>User of this group can update every public data.</td>
		</tr>	
		 <tr>
			<td class="first rowhead"><label>ADD:</label></td>
			<td class="value"><input type="checkbox" name="update_all" <?php echo $PERMISSION_ADD; ?> /></td>
			<td>User of this group can add public data.</td>
		</tr>	
		 <tr>
			<td class="first rowhead"><label>DELETE:</label></td>
			<td class="value"><input type="checkbox" name="update_status" <?php echo $PERMISSION_DELETE; ?> /></td>
			<td>User of this group can delete public data.</td>
		</tr>	
		 <tr>
			<td class="first rowhead"><label>ADMIN:</label></td>
			<td class="value"><input type="checkbox" name="admin" <?php echo $PERMISSION_ADMIN; ?> /></td>
			<td>User of this group can administrate dbms.</td>
		</tr>	
	</table>
	<br>
	<input type="submit" value="Save" />
	<input type="button" <?php echo $disabled; ?> value="Delete" onClick="location.href='index.php?page=admin&section=groups&method=delete_group&id=<?php echo $group_id; ?>'" />
	<input type="hidden" name="id" value=<?php echo $group_id; ?> />
	<input type="hidden" name="page" value="admin" />
	<input type="hidden" name="section" value="groups" />
	<input type="hidden" name="method" value=<?php echo $method; ?> />
</form>
