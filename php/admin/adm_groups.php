<?php  

	$groups = $controller->getGroups()->getData();
	$columns = array(
     	'id',
     	'name',
     	'permissions'
	);

	if(isset($_GET["action"]) && strlen($_GET["action"])) {
		include("php/admin/adm_groups_edit.php");
	} else {
		?>

		<h3>Groups</h3>
		<br />
		
		<table id="adm-table">
			<?php
				/* Generate table */
				echo "<thead>";
				echo "<tr>";
				foreach($columns as $column) {
					echo "<th>".$column."</th>";
				}
				echo "</tr>";
				echo "</thead>";

				echo "<tbody>";
				$count = 0;
				foreach ($groups as $key => $value) {

					$title = "id='".intval($value->getProperty("id"))."'";
					echo "<tr ".$title." title='groups'>";

					foreach($columns as $column) {
						echo "<td>".$value->getProperty($column)."</td>";
					}
					echo "</tr>";

					$count++;
				}
				echo "</tbody>";
			?>
		</table>

		<br />
		<input type="button" value="New Group" onClick="location.href='index.php?page=admin&section=groups&action=edit'" />

		<?php
	}
?>
