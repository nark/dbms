<!DOCTYPE html>
<meta charset="UTF-8" />
<html xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" media="screen" type="text/css" title="Design" href="css/reset.css" />
	<link rel="stylesheet" media="screen" type="text/css" title="Design" href="css/design.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="css/smoothness/jquery-ui-1.8.18.custom.css" />
	<link rel="stylesheet" media="screen" type="text/css" href="css/datatable.css" />

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
	<script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="js/functions.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<title>DBMS</title>
</head>

<body>
<div id="wrapper">

