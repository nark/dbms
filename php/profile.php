<?php

$user_id 			= null;
$target_user 		= null;

$login 				= null;
$fullname 			= "";
$email 				= "";

if(isset($_GET["id"]))
	$user_id = $_GET["id"];

if($user_id)
	$target_user = $controller->getUsers()->getObjectWithID($user_id);

if($target_user) {
	if($controller->getGroup()->getProperty("permissions") & PERMISSION_ADMIN) {
		$login 		= $target_user->getProperty("login");
	}
	$fullname 	= $target_user->getProperty("fullname");
	$email 		= $target_user->getMailLink();

	$columns = array(
 		'name'
	);
}
?>

<div id="content">
	<h2>User Info</h2>

	<div id="profile">
		<table border="0" bordercolor="" style="background-color:" width="400" cellpadding="0" cellspacing="">
			<?php if($login) { ?>
			<tr>
				<td><span class="label">Login:</span></td>
				<td><?php echo $login; ?></td>
			</tr>
			<?php } ?>
			<tr>
				<td><span class="label">Fullname:</span></td>
				<td><span class="editable"><?php echo $fullname; ?></span></td>
			</tr>
			<tr>
				<td><span class="label">Email:</span></td>
				<td><span class="editable"><?php echo $email; ?></span></td>
			</tr>
		</table>
	</div>
</div>