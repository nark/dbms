
<?php

	$sections = array(
     'general' 		=> 'php/admin/adm_general.php',
     'groups' 		=> 'php/admin/adm_groups.php',
     'users' 		=> 'php/admin/adm_users.php',
     'plugins' 		=> 'php/admin/adm_plugins.php',
     'languages' 	=> 'php/admin/adm_languages.php'
	);

?>

<div id="content">
	<nav>
		<?php
			if($user_group['permissions'] & PERMISSION_ADMIN) {
				foreach ($sections as $key => $value) {
					echo "<a href='index.php?page=admin&section=".$key."'>".ucfirst($key)."</a>";
				}
			}
		?>
	</nav>

	<div id="admin-content">
		<?php
			if($user_group['permissions'] & PERMISSION_ADMIN) {
				if(isset($_GET['section']) && $_GET['section'] < sizeof($sections)) {     
					include($sections[$_GET['section']]);
				} else {
					include($sections['general']);	
				}
			} else {
				echo "<p style='color:red;'>You do not have access to this content.</p>";
			}		
		?>
	</div>
</div>