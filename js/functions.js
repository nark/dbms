function cl(str) {
	console.log(str);
}

function selectRowInAdminTable(table) {
    var row_id 		= table.context.id;
    var table 		= table.context.title;
    window.location = "index.php?page=admin&section=" + table + "&action=edit&row_id=" + row_id;
}


function writeObj(obj, message) {
	if (!message) { message = obj; }
	var details = "*****************" + "\n" + message + "\n";
	var fieldContents;
	for (var field in obj) {
		fieldContents = obj[field];
		if (typeof(fieldContents) == "function") {
			fieldContents = "(function)";
		}
		details += "  " + field + ": " + fieldContents + "\n";
	}
	cl(details);
}