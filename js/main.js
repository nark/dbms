$(document).ready(function(){

    // admin table
    adminTable = $('#adm-table').dataTable({
        "bJQueryUI"         : true,
        "bStateSave"        : true,
        "iDisplayLength"    : 25,
        "aLengthMenu"       : [[25, 50, 100, -1], [25, 50, 100, "All"]],
        "sPaginationType"   : "full_numbers"
    });

    /* Add a click handler to the rows - this could be used as a callback */
    $("#adm-table tbody tr").click( function( e ) {
        if ( $(this).hasClass('row_selected') ) {
            $(this).removeClass('row_selected');
        }
        else {
            adminTable.$('tr.row_selected').removeClass('row_selected');
            $(this).addClass('row_selected');

            selectRowInAdminTable($(this));

        }
    });

});