<?php
/**
* PHP Class to user access (login, register, logout, etc)
*
* Additions by Rafaël Warnault with following features:
* - PDO support instead of native mysql
* - An object-oriented layer to handle SQL data as PHP objects
* 
* <code><?php
* include('session.sql.class.php');
* $controller = new SessionObject();
* ? ></code>
* 
* For support issues please refer to the webdigity forums :
*				http://www.webdigity.com/index.php/board,91.0.html
* or the official web site:
*				http://phpUserClass.com/
* ==============================================================================
* 
* @version $Id: access.class.php,v 0.93 2008/05/02 10:54:32 $
* @copyright Copyright © 2007 Nick Papanotas (http://www.webdigity.com)
* @copyright Copyright © 2012 Rafaël Warnault (http://www.read-write.fr)
* @author Nick Papanotas <nikolas@webdigity.com>
* @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
* 
* ==============================================================================
*
*/

require_once 'object.sql.class.php';


/**
* Session Object - The SQL Session Class
* 
* @param string $dbName
* @param string $dbHost 
* @param string $dbUser
* @param string $dbPass
* @param string $table
*/
class SessionObject extends SQLObject {

	/*Settings*/
	var $dbDriver 	= 'mysql';
	var $dbName 	= 'dbms';
	var $dbHost 	= 'localhost';
	var $dbPort 	= 3306;
	var $dbUser 	= 'root';
	var $dbPass 	= 'root';
	var $table  	= 'users';

	/**
	* The session variable ($_SESSION[$sessionVariable]) which will hold the data while the user is logged on
	*/
	var $sessionVariable = 'userSessionValue';

	/**
	* Those are the fields that our table uses in order to fetch the needed data. The structure is 'fieldType' => 'fieldName'
	*/
	var $tbFields = array(
		'userID'	=> 'id', 
		'login' 	=> 'login',
		'pass'  	=> 'password',
		'email' 	=> 'email',
		'active'	=> 'active');

		/**
		* When user wants the system to remember him/her, how much time to keep the cookie? (seconds)
		* var int
		*/
		var $remTime = 2592000;//One month
		/**
		* The name of the cookie which we will use if user wants to be remembered by the system
		* var string
		*/
		var $remCookieName = 'CarimSavePass';
		/**
		* The cookie domain
		* var string
		*/
		var $remCookieDomain = '';
		/**
		* The method used to encrypt the password. It can be sha1, md5 or nothing (no encryption)
		* var string
		*/
		var $passMethod = 'sha1';
		/**
		* Display errors? Set this to true if you are going to seek for help, or have troubles with the script
		* var bool
		*/
		var $displayErrors = true;
		/*Do not edit after this line*/
		var $userID;

		/**
		* Class Constructure
		* 
		* @param string $connection
		* @param array $settings
		* @return void
		*/
		public function __construct($table, $connection, $data = NULL, $id = NULL) {

			// if ( is_array($settings) ){
			// 	foreach ( $settings as $k => $v ){
			// 		if ( !isset( $this->{$k} ) ) die('Property '.$k.' does not exists. Check your settings.');
			// 		$this->{$k} = $v;
			// 	}
			// }

			$this->remCookieDomain 	= $this->remCookieDomain == '' ? $_SERVER['HTTP_HOST'] : $this->remCookieDomain;

			try {
				$this->connection = new PDO($this->dbDriver.":host=".$this->dbHost.";port=".$this->dbPort.";dbname=".$this->dbName, $this->dbUser, $this->dbPass);   
				$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch(PDOException $e) {
				echo $e->getMessage();
			}

			if( !isset( $_SESSION ) ) 
			session_start();

			if(!empty($_SESSION[$this->sessionVariable]) )
			$this->loadUser( $_SESSION[$this->sessionVariable] );

			//Maybe there is a cookie?
			if ( isset($_COOKIE[$this->remCookieName]) && !$this->is_loaded()){
				//echo 'I know you<br />';
				$u = unserialize(base64_decode($_COOKIE[$this->remCookieName]));
				$this->login($u['uname'], $u['password']);
			}
		}



		/**
		* Login function
		* @param string $uname
		* @param string $password
		* @param bool $loadUser
		* @return bool
		*/
		function login($uname, $password, $remember = false, $loadUser = true)
		{
			// escape username and password
			$uname    = $this->escape($uname);
			$password = $originalPassword = $this->escape($password);

			// encode password with the desired pass method, sha1 or md5
			switch(strtolower($this->passMethod)){
				case 'sha1':
				$password = "SHA1($password)"; break;
				case 'md5' :
				$password = "MD5($password)";break;
				case 'nothing':
				$password = "'$password'";
			}

			// format the user query
			$res = $this->query("SELECT * FROM {$this->table} 
			WHERE {$this->tbFields['login']} = {$uname} 
			AND {$this->tbFields['pass']} = $password 
			LIMIT 1",__LINE__);

			if (count($res) == 0)
			return false;
			
			if ($loadUser) {
				foreach ($res as $row) {
					$this->data = $row;
					$this->userID = $this->data[$this->tbFields['userID']];
					$this->id = $this->userID;

					$_SESSION[$this->sessionVariable] = $this->userID;

					if ($remember) {
						$cookie 	= base64_encode(serialize(array('uname'=>$uname,'password'=>$originalPassword)));
						$a 			= setcookie($this->remCookieName, $cookie, time()+$this->remTime, '/', $this->remCookieDomain);
					}
				}
			}
			return true;
		}

		/**
		* Logout function
		* param string $redirectTo
		* @return bool
		*/
		function logout($redirectTo = '')
		{
			setcookie($this->remCookieName, '', time()-3600);
			$_SESSION[$this->sessionVariable] = '';
			$this->data = '';
			if ( $redirectTo != '' && !headers_sent()){
				header('Location: '.$redirectTo );
				exit;//To ensure security
			}
		}
		/**
		* Function to determine if a property is true or false
		* param string $prop
		* @return bool
		*/
		function is($prop){
			return $this->getProperty($prop)==1?true:false;
		}

		/**
		* Is the user an active user?
		* @return bool
		*/
		function is_active()
		{
			return $this->data[$this->tbFields['active']];
		}

		/**
		* Is the user loaded?
		* @ return bool
		*/
		function is_loaded() {
			return empty($this->userID) ? false : true;
		}
		/**
		* Activates the user account
		* @return bool
		*/
		function activate()
		{
			// if (empty($this->userID)) $this->error('No user is loaded', __LINE__);
			// if ( $this->is_active()) $this->error('Allready active account', __LINE__);

			// $res = $this->query("UPDATE `{$this->table}` SET {$this->tbFields['active']} = 1 
			// WHERE `{$this->tbFields['userID']}` = '".$this->escape($this->userID)."' LIMIT 1");

			// if (@mysql_affected_rows() == 1)
			// {
				// 	$this->data[$this->tbFields['active']] = true;
				// 	return true;
				// }
				return false;
			}
			/*
			* Creates a user account. The array should have the form 'database field' => 'value'
			* @param array $data
			* return int
			*/  
			function insertUser($data){
				// if (!is_array($data)) $this->error('Data is not an array', __LINE__);
				// switch(strtolower($this->passMethod)){
					// 	case 'sha1':
					// 	$password = "SHA1('".$data[$this->tbFields['pass']]."')"; break;
					// 	case 'md5' :
					// 	$password = "MD5('".$data[$this->tbFields['pass']]."')";break;
					// 	case 'nothing':
					// 	$password = $data[$this->tbFields['pass']];
					// }
					// foreach ($data as $k => $v ) $data[$k] = "'".$this->escape($v)."'";
					// $data[$this->tbFields['pass']] = $password;
					// $this->query("INSERT INTO `{$this->table}` (`".implode('`, `', array_keys($data))."`) VALUES (".implode(", ", $data).")");
					// return (int)mysql_insert_id($this->connection);
				}
				/*
				* Creates a random password. You can use it to create a password or a hash for user activation
				* param int $length
				* param string $chrs
				* return string
				*/
				function randomPass($length=10, $chrs = '1234567890qwertyuiopasdfghjklzxcvbnm'){
					for($i = 0; $i < $length; $i++) {
						$pwd .= $chrs{mt_rand(0, strlen($chrs)-1)};
					}
					return $pwd;
				}


				/**
				* A function that is used to load one user's data
				* @access private
				* @param string $userID
				* @return bool
				*/
				function loadUser($userID)
				{
					$res = $this->query("SELECT * FROM {$this->table} WHERE {$this->tbFields['userID']} = ".$this->escape($userID)." LIMIT 1");

					if (count($res) == 0)
					return false;

					foreach ($res as $row)
					$this->data = $row;

					$this->userID 	= $userID;
					$this->id 		= $userID;

					$_SESSION[$this->sessionVariable] = $this->userID;
					return true;
				}

				/**
				* Error holder for the class
				* @access private
				* @param string $error
				* @param int $line
				* @param bool $die
				* @return bool
				*/  
				function error($error, $line = '', $die = false) {
					if ( $this->displayErrors )
					echo '<b>Error: </b>'.$error.'<br /><b>Line: </b>'.($line==''?'Unknown':$line).'<br />';
					if ($die) exit;
					return false;
				}
			};
			?>