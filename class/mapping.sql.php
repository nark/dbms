<?php


/* GROUPS TABLE */
$groups_table_map = array(
		'id',
		'name',
		'permissions'
);


/* USERS TABLE */
$users_table_map = array(
		'id',
		'login',
		'password',
		'fullname',
		'email',
		'active',
		'group_id'
);



/* TABLE MAPS */
$table_maps = array(
		'groups' 			=> $groups_table_map,
		'users' 			=> $users_table_map
);


/* CLASS MAPS */
$class_maps = array(
		'groups' 			=> 'GroupObject',		
		'users' 			=> 'UserObject'
);




function map_for_table($table) {
	global $table_maps;
	if(!isset($table_maps[$table])) {
		return NULL;
	}

	return $table_maps[$table];
}

function class_for_table($table) {
	global $class_maps;
	if(!isset($class_maps[$table])) {
		return NULL;
	}

	return $class_maps[$table];
}

?>