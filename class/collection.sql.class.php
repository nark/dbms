<?php 
include_once 'mapping.sql.php';
require_once 'object.sql.class.php';
require_once 'controller.sql.class.php';
require_once 'group.sql.class.php';
require_once 'user.sql.class.php';


/**
 * Collection inherit from SQLObject to provide
 * an object wrapper for SQL collections of data
 */
class Collection extends SQLObject {
		
	protected $filters		= NULL; // filters applied in reloadData
	protected $sortby 		= NULL; // sort by applied in reloadData
	protected $limit 		= NULL; // limit of rows applied in reloadData
	protected $class 		= NULL;
	protected $target_table = NULL;

	/** 
	 * Overwritte the constructor to make the data attribute
	 * an initialized array object
	 */
	public function __construct($table, $connection, $data = NULL, $id = NULL, $filters = NULL, $sortby = NULL, $limit = NULL) {

		$this->connection 	= $connection;
		$this->table 		= $table;
		$this->data 		= array();

		$this->filters 		= $filters;
		$this->sortby 		= $sortby;
		$this->limit 		= $limit;

		$target_tables 		= explode(',', $this->table);
		$this->target_table = $target_tables[0];

		$this->class 		= class_for_table($this->target_table);
		$this->map 			= map_for_table($this->target_table);

		$this->reloadData();
	}


	/** 
	 * Overwritte reloadData to relaod collection 
	 * instead object from the database 
	 */
	public function reloadData() {
		// init/clean the data array
		$this->data = array();

		// format filters for SQL query
		$filters = "";
		if($this->filters) {
			$filters 	= "WHERE ";
			$count 		= 0;
			
			foreach ($this->filters as $key => $value) {
    			$filters = $filters . $key . "=" .$value;
    			
    			if(count($this->filters) != $count+1)
    				$filters = $filters . " AND ";

    			$count++;
			}
		}

		$to_select = implode(",", $this->map);
    	$sql  = "SELECT ".$to_select." FROM {$this->table} ".$filters;

    	// append sort by
    	if($this->sortby) {
    		$sql = $sql . " ORDER BY " . $this->sortBy();
    	}

    	// append limit
		if($this->limit) {
    		$sql = $sql . " LIMIT " . $this->limit();
    	}

    	$target_tables = explode(',', $this->table);
    	$target_table  = $target_tables[0];

    	$stmt 	= $this->query($sql);
    	$i 		= 0;

    	while ($result = $stmt->fetch(PDO::FETCH_ASSOC)) {
    		$this->data[$i] = new $this->class($target_table, $this->connection, $result, $result['id']);
	    	$i++;
    	}
	}


	/** Collection Management Methods */
	public function getObjectWithID($object_id) {
		foreach ($this->getData() as $object) {
			if($object->getProperty('id') == $object_id) {
				return $object;
			}
		}
		return NULL;
	}

	public function getObjectForKeyValue($object_key, $object_value) {
		foreach ($this->getData() as $object) {
			foreach ($object as $key => $value) {
				if($key == $object_key && $value == $object_value) {
					return $object;
				}
			}
		}
		return NULL;
	}

	public function addObject($object) {
		
	}

	public function removeObjectWithID($object_id) {
		
	}

	public function removeObject($object) {
		
	}


	/** JSON helpers */ 
	public function encodeJSON() {
		$datas 		= $this->getData();
		$response 	= "{ \"response\": [";
		$count 		= 0;

		if($datas) {
			foreach ($datas as $data) {
				$response = $response . $data->encodeJSON();

				if(sizeof($datas) != $count+1)
					$response = $response . ", ";

				$count++;
			}

			$response = $response . "]}";

			return $response;
		}
		return NULL;
	}



	/** Accessors Methods */
	public function setFilters($filters) {
		$this->filters = $filters;
		$this->reloadData();
	}
	
	public function filters() {
		return $this->filters;
	}
	
	public function setSortBy($sortby) {
		$this->sortby = $sortby;
		$this->reloadData();
	}
	
	public function sortBy() {
		return $this->sortby;
	}

	public function setLimit($limit) {
		$this->limit = $limit;
		$this->reloadData();
	}
	
	public function limit() {
		return $this->limit;
	}
}


?>