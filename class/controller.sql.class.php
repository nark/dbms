<?php

require_once 'session.sql.class.php';
require_once 'collection.sql.class.php';
require_once 'user.sql.class.php';


// groups & permissions
define('PERMISSION_DENIED', 		1 << 0);
define('PERMISSION_READ', 			1 << 1);
define('PERMISSION_UPDATE',  		1 << 2);
define('PERMISSION_ADD',			1 << 3);
define('PERMISSION_DELETE',			1 << 4);
define('PERMISSION_ADMIN', 			1 << 5);



class Controller extends SessionObject {

	private $_groups				= NULL;
	private $_users 				= NULL;
	private $_group 				= NULL;
	private $_breadcrumb 			= NULL;	
	
	
	/** Breadcrumb management */
	public function setBreadcrumb($breadcrumb) {
		$this->_breadcrumb = $breadcrumb;
	}
	
	public function breadcrumb() {
		return $this->_breadcrumb;
	}
	


	/** 
	 * Returns an array containing all database users
	 * NOTE: permissions aren't managed here yet...
	 */
	public function getUsers() {
		if(!$this->_users) {
			$this->_users = new Collection("users", $this->connection);
		}
		
		return $this->_users;
	}
	


	/** 
	 * Returns the current user group
	 */
	public function getGroup() {

		if(!$this->_group) {
			$this->_group = new SQLObject("groups", $this->connection, NULL, $this->getProperty('group_id'));
			$this->_group->reloadData();
		}
		return $this->_group;
	}

	/** 
	 * Returns an array containing all database groups
	 * NOTE: permissions aren't managed here yet...
	 */
	public function getGroups() {

		if(!$this->_groups) {
			$this->_groups = new Collection("groups", $this->connection);
		}
		
		return $this->_groups;
	}

	/** 
	 * Returns a group object for a given group ID
	 * NOTE: permissions aren't managed here yet...
	 */
	public function getGroupWithID($id) {
		foreach ($this->getGroups()->getData() as $group) {
			if($group->getProperty('id') == $id)
				return $group;
		}
		return NULL;
	}
};
?>