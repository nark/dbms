<?php 
require_once 'object.sql.class.php';

class UserObject extends SQLObject {

	/** 
	 * Returns a formated link to the user profile
	 * based on user fullname
	 */
	public function getProfileLink() {
		return "<a href='index.php?page=profile&id=".$this->getProperty('id')."'>".$this->getProperty('fullname')."</a>";
	}
	
	/** 
	 * Returns a formated mailto link to the user address
	 */
	public function getMailLink() {
		$mail = $this->getProperty("email");
		if($mail)
			return "<a href='mailto:".$mail."'>".$mail."</a>";
		return "";
	}
};

?>