<?php 
include_once 'mapping.sql.php';

abstract class SQLAbstract {
	protected 	 $connection	= null;
	protected 	 $table			= null;
	protected 	 $map			= null;
	protected 	 $class			= null;
	protected 	 $data			= null;
	protected 	 $id			= null;

	// 
	//abstract protected static className();

	// constructor
	public function __construct($table, $connection, $data = NULL, $id = NULL) {
		$this->connection 	= $connection;
		$this->table 		= $table;
		$this->data 		= $data;
		$this->id 			= $id;

		// load the matching map for the table of the class
		$this->map 			= map_for_table($this->table);
		$this->class 		= class_for_table($this->table);
	}

	// reload the object content from the database
	abstract public function reloadData();

	// save the object to the database (private, only available on API side)
	abstract public function saveObject();

	// accessors
	abstract public function getProperty($property);
	abstract public function setProperties($properties);



	/** Common Accessors */
	public function getConnection() {
		return $this->connection;
	}

	public function setConnection($connection) {
		$this->connection = $connection;
	}

	public function getTable() {
		return $this->table;
	}

	public function setTable($table) {
		$this->table = $table;
	}

	public function getData() {
		return $this->data;
	}

	public function setData($data) {
		$this->data = $data;
	}

	public function getMap() {
		return $this->map;
	}

	public function setMap($map) {
		$this->map = $map;
	}


	public function getID() {
		return $this->id;
	}

	public function setID($id) {
		$this->id = $id;
	}



	// sql helpers
	public function query($sql, $line = 'Uknown') {
		try {
			$result = $this->connection->query($sql);         
			return $result;

		} catch(PDOException $e) {
			echo $e->getMessage();
		}
		return NULL;
	}

	public function exec($sql, $line = 'Uknown') {
		try {
			$result = $this->connection->exec($sql);
			return $result;

		} catch(PDOException $e) {
			echo $e->getMessage();
		}
		return false;
	}

	public function escape($str) {
		$str = get_magic_quotes_gpc() ? stripslashes($str) : $str;
		$str = $this->connection->quote($str);
		return $str;
	}



	// json helpers
	public function encodeJSON() 
	{ 
	    foreach ($this as $key => $value) 
	    { 
	        $json->$key = $value; 
	    } 
	    return json_encode($json); 
	} 
	
	public function decodeJSON($json_str) 
	{ 
	    $json = json_decode($json_str, 1); 
	    foreach ($json as $key => $value) 
	    { 
	        $this->$key = $value; 
	    } 
	} 

}

?>