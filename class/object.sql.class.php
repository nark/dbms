<?php 

require_once 'abstract.sql.class.php';
include_once 'mapping.sql.php';


class SQLObject extends SQLAbstract {

	private $updated = false;


	// reload the object from the database
	public function reloadData() {
		$to_select 	= implode(",", $this->map);		
		$sql  		= "SELECT ".$to_select." FROM ".$this->table." WHERE id=".$this->id." LIMIT 1";

		$results = $this->query($sql);
		if(!$results)
			return NULL;  

		if(count($results) == 0)
			return NULL;  

		foreach ($results as $row)
			$this->data = $row;
	}  


	// save the object to the database
	public function saveObject() {
		// if object has been updated, update the database
		if($this->updated) {
			$sql 	= "UPDATE ".$this->table." SET ";
			$count 	= 0;

			foreach ($this->map as $key) {
				$sql = $sql . $key . "=" .$this->escape($this->getProperty($key));
				if(sizeof($this->map) != $count+1)
					 	$sql = $sql . ", ";
				$count++;
			}
			$sql = $sql . " WHERE id=" .$this->getProperty("id");
			$this->updated = false;
			$this->connection->exec($sql);

		// if object was newly created, insert into the database
		} else if(!isset($this->data['id'])){
			
			$sql 	= "INSERT INTO ".$this->table." (";
			$count 	= 0;

			foreach ($this->data as $key => $value) {
				$sql = $sql . $key;
				if(sizeof($this->data) != $count+1)
					 $sql = $sql . ", ";
				$count++;
			}

			$sql = $sql . ") VALUES (";
			$count = 0;

			foreach ($this->data as $key => $value) {
				$sql = $sql . $this->escape($value);

				if(sizeof($this->data) != $count+1)
					 	$sql = $sql . ", ";
				$count++;
			}

			$sql = $sql . ")";
		
			if($this->connection->exec($sql)) {
				$last_id = $this->connection->lastInsertId();
				
				$this->id = $last_id;
				$this->data["id"] = $last_id;
			}
		}
	}


	// accessors
	public function getProperty($property) {
		if(!isset($this->data[$property])) 
			return null;

		return $this->data[$property];
	}

	public function setProperties($properties) {
		foreach ($properties as $key => $value) {
			if(isset($this->data[$key]))
				$this->data[$key] = $value;
		}

		// save the object to the database
		$this->updated = true;
		$this->saveObject();
	}
	
}

?>