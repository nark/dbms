<?php


// an enumeration of type method (GET or POST)
class MethodType {
	// unknow method
	const UNKNOW_METHOD		= 'unknow';

	// session managment
    const LOGIN       		= 'login';
    const LOGOUT	  		= 'logout';

    // groups managment
    const NEW_GROUP	  		= 'new_group';
    const SAVE_GROUP  		= 'save_group';
    const DELETE_GROUP		= 'delete_group';
};


$methods_permissions = array(
	MethodType::LOGIN 				=> PERMISSION_DENIED, 
	MethodType::LOGOUT 				=> PERMISSION_DENIED, 
	MethodType::NEW_GROUP 			=> PERMISSION_ADMIN,
	MethodType::SAVE_GROUP 			=> PERMISSION_ADMIN,
	MethodType::DELETE_GROUP  		=> PERMISSION_ADMIN
);


// listen to handle GET methods
cm_method_dispatch_get();



// dispatch POST methods
function cm_method_dispatch_post() {
	if(isset($_POST['method']) && strlen($_POST['method']) > 0) {
		$method = $_POST['method'];
			
		// dispatch POST methods
		cm_method_dispatch($_POST);	
	}
}



// dispatch GET methods
function cm_method_dispatch_get() {
	if(isset($_GET['method']) && strlen($_GET['method']) > 0) {
		$method = $_GET['method'];

		// dispatch GET methods
		cm_method_dispatch($_GET);
	}
}


// dispatch GET methods
function cm_method_dispatch($_GET_OR_POST) {
	global $controller;
	global $methods_permissions;

	// check for method parameter
	if(isset($_GET_OR_POST['method']) && strlen($_GET_OR_POST['method']) > 0) {
		$method = $_GET_OR_POST['method'];

		// check for permissions
		if($method == MethodType::LOGIN || $controller->getGroup()->getProperty("permissions") & $methods_permissions[$method]) {
			
			// enumerate around known methods const defined into MethodType
			switch($method) {
				// session management
				case MethodType::LOGIN: 		cm_method_login($_GET_OR_POST);	 		break;
				case MethodType::LOGOUT: 		cm_method_logout($_GET_OR_POST); 		break;

				// group management
				case MethodType::NEW_GROUP: 	cm_method_new_group($_GET_OR_POST); 	break;
				case MethodType::SAVE_GROUP: 	cm_method_save_group($_GET_OR_POST); 	break;
				case MethodType::DELETE_GROUP: 	cm_method_delete_group($_GET_OR_POST); 	break;
			
				default: echo "<p style='color:red;'>Error: Unknow method</p>";
			}
			
			// update the breadcrumb with new received GET fields
			cm_update_breadcrumb($_GET);	
		}
	}
}




// login method handler
function cm_method_login($_GET_OR_POST) {	
	global $controller;
	
	$login 		= null;
	$password 	= null;
	$remember	= false;
	
	// check login post parameters
	if(isset($_GET_OR_POST['login']) && strlen($_GET_OR_POST['login']) > 0)
		$login = $_GET_OR_POST['login'];
		
	// check password post parameters
	if(isset($_GET_OR_POST['password']) && strlen($_GET_OR_POST['password']) > 0)
		$password = $_GET_OR_POST['password'];
	
	if(isset($_GET_OR_POST['remember']))	
		$remember = $_GET_OR_POST['remember'];

	if($controller->login($login, $password, $remember)) {
		redirect();
	} else {
		echo 'Wrong username and/or password';
	}
}



// logout method handler
function cm_method_logout($_GET_OR_POST) {
	global $controller;	
	
	$controller->logout('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
}




// create a new group
function cm_method_new_group($_GET_OR_POST) {
	global $controller;

	$name = NULL;
	$permissions	= PERMISSION_DENIED;

	$read 			= false;
	$update_own 	= false;
	$update_all 	= false;
	$update_status 	= false;
	$admin 			= false;

	if(isset($_GET_OR_POST['name']))
		$name = $_GET_OR_POST['name'];

	if(isset($_GET_OR_POST['read']))
		$permissions = ($permissions | PERMISSION_READ);

	if(isset($_GET_OR_POST['update_own']))
		$permissions = ($permissions | PERMISSION_UPDATE);

	if(isset($_GET_OR_POST['update_all']))
		$permissions = ($permissions | PERMISSION_ADD);

	if(isset($_GET_OR_POST['update_status']))
		$permissions = ($permissions | PERMISSION_DELETE);

	if(isset($_GET_OR_POST['admin']))
		$permissions = ($permissions | PERMISSION_ADMIN);

	$properties = array('name' => $name, 'permissions' => $permissions);
	$group = new GroupObject("groups", $controller->getConnection(), $properties);
	$group->saveObject();

	// $sql = "INSERT INTO groups(name, permissions) VALUES ('".$name."', '".$permissions."')";
	// $controller->exec($sql);
}


// save/update a group 
function cm_method_save_group($_GET_OR_POST) {
	global $controller;

	// UPDATE animals SET animal_name='bruce' WHERE animal_name='troy'

	if(isset($_GET_OR_POST['id'])) {

		$id 			= NULL;
		$name 			= NULL;
		$permissions	= PERMISSION_DENIED;

		$read 			= false;
		$update_own 	= false;
		$update_all 	= false;
		$update_status 	= false;
		$admin 			= false;

		if(isset($_GET_OR_POST['id']))
			$id = $_GET_OR_POST['id'];

		if(isset($_GET_OR_POST['name']))
			$name = $_GET_OR_POST['name'];

		if(isset($_GET_OR_POST['read']))
			$permissions = ($permissions | PERMISSION_READ);

		if(isset($_GET_OR_POST['update_own']))
			$permissions = ($permissions | PERMISSION_UPDATE);

		if(isset($_GET_OR_POST['update_all']))
			$permissions = ($permissions | PERMISSION_ADD);

		if(isset($_GET_OR_POST['update_status']))
			$permissions = ($permissions | PERMISSION_DELETE);

		if(isset($_GET_OR_POST['admin']))
			$permissions = ($permissions | PERMISSION_ADMIN);

		$group = $controller->getGroups()->getObjectWithID($id);
		$properties = array('name' => $name, 'permissions' => $permissions);
		$group->setProperties($properties);
	}
}

// delete a group
function cm_method_delete_group($_GET_OR_POST) {
	// DELETE FROM votre_table WHERE nom = 'Martin'
	global $controller;

	if(isset($_GET_OR_POST['id'])) {
		$sql = "DELETE FROM groups WHERE id ='".$_GET_OR_POST['id']."'";
		$controller->exec($sql);
	}
}



// update the user breadcrumb field
function cm_update_breadcrumb($_GET_OR_POST) {

	global $controller;

	$breadcrumb = "<a href='index.php'>Home</a>";
	
	if(isset($_GET_OR_POST['page'])) {
		$page = $_GET_OR_POST['page'];

		$breadcrumb = $breadcrumb . " > ";
		
		if($page == "profile") {
			if (isset($_GET_OR_POST["id"])) {
				$the_user = $controller->getUsers()->getObjectWithID($_GET_OR_POST["id"]);
				if ($the_user) {
					$breadcrumb = $breadcrumb . "<a href='index.php?page=".$page."'>".$the_user->getProfileLink()."</a>";
				}
			}
		} else {
			$breadcrumb = $breadcrumb . "<a href='index.php?page=".$page."'>".ucfirst($page)."</a>";
		}

		if(isset($_GET_OR_POST['section'])) {
			$section = $_GET_OR_POST['section'];

			// if(strpos($section, "_edit") !== false)
			// 	$section = substr_replace('_edit', $section, 0);
	
			$breadcrumb = $breadcrumb . " > ";
			$breadcrumb = $breadcrumb . "<a href='index.php?page=".$page."&section=".$section."'>".ucfirst($section)."</a>";
		}
	}	
	
	$controller->setBreadcrumb($breadcrumb);
}




// redirect to the index page (used in cm_method_login funtion only)
function redirect() {
	header('Location: http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
}




?>