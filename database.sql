-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Dim 08 Avril 2012 à 00:40
-- Version du serveur: 5.5.9
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `dbms`
--

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE `groups` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `permissions` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=5 ;

--
-- Contenu de la table `groups`
--

INSERT INTO `groups` VALUES(1, 'admin', 63);
INSERT INTO `groups` VALUES(2, 'manager', 31);
INSERT INTO `groups` VALUES(3, 'reporter', 23);
INSERT INTO `groups` VALUES(4, 'visitor', 3);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `fullname` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `group_id` int(255) NOT NULL,
  `active` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`,`fullname`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=6 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` VALUES(1, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'admin@dbms.com', 1, 1);
INSERT INTO `users` VALUES(2, 'manager', '1a8565a9dc72048ba03b4156be3e569f22771f23', 'Manager', 'manager@dbms.com', 2, 1);
INSERT INTO `users` VALUES(3, 'reporter', '40a630e157504605e40ba241f6b1f78ab1dd97b9', 'Reporter', 'reporter@dbms.com', 3, 1);
INSERT INTO `users` VALUES(4, 'visitor', '4ed0428505b0b89fe7bc1a01928ef1bd4c77c1be', 'Visitor', 'visitor@dbms.com', 4, 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
