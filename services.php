<?php
require_once 'class/controller.sql.class.php';


/**
 * MethodType class enumerates Web Service method names
 */
class MethodType {
	const GET_USERS      = 'get_users';
};



// create new session
$controller = new Controller("users", NULL);

// auth the web sevice
if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
	if(!$controller->is_loaded()) {
		if($controller->login($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {

		} else {
			cm_services_auth_failed();
		}
	}
	cm_services_dispatch_methods();

} else {
	cm_services_auth_failed();
}




/**
 * Dispatch and check received Web Service methods
 */
function cm_services_dispatch_methods() {
	global $controller;

	if(isset($_POST['method'])) {
		$method = $_POST['method'];

		switch ($method) {
			case MethodType::GET_USERS:  cm_services_get_users($_POST); break;

			default: cm_services_reply_error("Unknow method: ".$method); break;
		}
	}
}





/**
 * Reply user authentification failed
 */
function cm_services_auth_failed() {
	global $controller;
	cm_services_reply_error("Authentification failed for user: ".$_SERVER['PHP_AUTH_USER']);
}   

/**
 * Reply OK
 */
function cm_services_reply_ok() {
	global $controller;
	$response = array('response' => "OK");
	echo json_encode($response);
}  

/**
 * Reply errors
 */
function cm_services_reply_error($error) {
	global $controller;
	$response = array('error' => $error);
	echo json_encode($response);
}  





/**
 * Get Users method
 */
function cm_services_get_users() {
	global $controller;
	$users = $controller->getUsers()->encodeJSON();

	if($users)
		echo $users;
}


?>