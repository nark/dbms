<?
require_once 'class/controller.sql.class.php';

// create new session
$controller  		= new Controller("users", NULL);
//$user->reloadData();

// subpages
$pages = array(
     'profile' 		=> 'php/profile.php',
     'content' 		=> 'php/content.php',
     'admin' 		=> 'php/admin.php'
);
	
// listen GET 
include_once("core.php");	

if (!$controller->is_loaded()) {
	
	// listen POST for login form
	cm_method_dispatch_post();
	
	// include the login form
	include_once("login.php");
	
} else {

	// html header
	include_once("php/head.php");
	
	// include the page header
	include_once("php/header.php");
		
	// include the breadcrumb
	cm_update_breadcrumb($_GET);
	include_once("php/toolbar.php");
	$user_group = $controller->getGroup()->getData();
	
	if($user_group['permissions'] & PERMISSION_READ) {
		if(isset($_GET['page']) && $_GET['page'] < sizeof($pages)) {     
			include($pages[$_GET['page']]);
		} else {
			include('php/content.php');	
		}		
	}  else {
		echo "<p style='color:red;'>You do not have access to this content.</p>";
	}	
	  	
  	// html footer
  	include_once("php/footer.php");
}

?>	